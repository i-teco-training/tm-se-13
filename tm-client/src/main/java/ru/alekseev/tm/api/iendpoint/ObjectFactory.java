
package ru.alekseev.tm.api.iendpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.alekseev.tm.api.iendpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AbstractEntity_QNAME = new QName("http://iendpoint.api.tm.alekseev.ru/", "abstractEntity");
    private final static QName _AddUser_QNAME = new QName("http://iendpoint.api.tm.alekseev.ru/", "addUser");
    private final static QName _AddUserByLoginAndPassword_QNAME = new QName("http://iendpoint.api.tm.alekseev.ru/", "addUserByLoginAndPassword");
    private final static QName _AddUserByLoginAndPasswordResponse_QNAME = new QName("http://iendpoint.api.tm.alekseev.ru/", "addUserByLoginAndPasswordResponse");
    private final static QName _AddUserByLoginPasswordUserRole_QNAME = new QName("http://iendpoint.api.tm.alekseev.ru/", "addUserByLoginPasswordUserRole");
    private final static QName _AddUserByLoginPasswordUserRoleResponse_QNAME = new QName("http://iendpoint.api.tm.alekseev.ru/", "addUserByLoginPasswordUserRoleResponse");
    private final static QName _AddUserResponse_QNAME = new QName("http://iendpoint.api.tm.alekseev.ru/", "addUserResponse");
    private final static QName _DeleteUser_QNAME = new QName("http://iendpoint.api.tm.alekseev.ru/", "deleteUser");
    private final static QName _DeleteUserByLoginAndPassword_QNAME = new QName("http://iendpoint.api.tm.alekseev.ru/", "deleteUserByLoginAndPassword");
    private final static QName _DeleteUserByLoginAndPasswordResponse_QNAME = new QName("http://iendpoint.api.tm.alekseev.ru/", "deleteUserByLoginAndPasswordResponse");
    private final static QName _DeleteUserResponse_QNAME = new QName("http://iendpoint.api.tm.alekseev.ru/", "deleteUserResponse");
    private final static QName _FindOneUser_QNAME = new QName("http://iendpoint.api.tm.alekseev.ru/", "findOneUser");
    private final static QName _FindOneUserByLoginAndPassword_QNAME = new QName("http://iendpoint.api.tm.alekseev.ru/", "findOneUserByLoginAndPassword");
    private final static QName _FindOneUserByLoginAndPasswordResponse_QNAME = new QName("http://iendpoint.api.tm.alekseev.ru/", "findOneUserByLoginAndPasswordResponse");
    private final static QName _FindOneUserResponse_QNAME = new QName("http://iendpoint.api.tm.alekseev.ru/", "findOneUserResponse");
    private final static QName _Session_QNAME = new QName("http://iendpoint.api.tm.alekseev.ru/", "session");
    private final static QName _UpdateUser_QNAME = new QName("http://iendpoint.api.tm.alekseev.ru/", "updateUser");
    private final static QName _UpdateUserByRole_QNAME = new QName("http://iendpoint.api.tm.alekseev.ru/", "updateUserByRole");
    private final static QName _UpdateUserByRoleResponse_QNAME = new QName("http://iendpoint.api.tm.alekseev.ru/", "updateUserByRoleResponse");
    private final static QName _UpdateUserPassword_QNAME = new QName("http://iendpoint.api.tm.alekseev.ru/", "updateUserPassword");
    private final static QName _UpdateUserPasswordResponse_QNAME = new QName("http://iendpoint.api.tm.alekseev.ru/", "updateUserPasswordResponse");
    private final static QName _UpdateUserResponse_QNAME = new QName("http://iendpoint.api.tm.alekseev.ru/", "updateUserResponse");
    private final static QName _User_QNAME = new QName("http://iendpoint.api.tm.alekseev.ru/", "user");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.alekseev.tm.api.iendpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AddUser }
     * 
     */
    public AddUser createAddUser() {
        return new AddUser();
    }

    /**
     * Create an instance of {@link AddUserByLoginAndPassword }
     * 
     */
    public AddUserByLoginAndPassword createAddUserByLoginAndPassword() {
        return new AddUserByLoginAndPassword();
    }

    /**
     * Create an instance of {@link AddUserByLoginAndPasswordResponse }
     * 
     */
    public AddUserByLoginAndPasswordResponse createAddUserByLoginAndPasswordResponse() {
        return new AddUserByLoginAndPasswordResponse();
    }

    /**
     * Create an instance of {@link AddUserByLoginPasswordUserRole }
     * 
     */
    public AddUserByLoginPasswordUserRole createAddUserByLoginPasswordUserRole() {
        return new AddUserByLoginPasswordUserRole();
    }

    /**
     * Create an instance of {@link AddUserByLoginPasswordUserRoleResponse }
     * 
     */
    public AddUserByLoginPasswordUserRoleResponse createAddUserByLoginPasswordUserRoleResponse() {
        return new AddUserByLoginPasswordUserRoleResponse();
    }

    /**
     * Create an instance of {@link AddUserResponse }
     * 
     */
    public AddUserResponse createAddUserResponse() {
        return new AddUserResponse();
    }

    /**
     * Create an instance of {@link DeleteUser }
     * 
     */
    public DeleteUser createDeleteUser() {
        return new DeleteUser();
    }

    /**
     * Create an instance of {@link DeleteUserByLoginAndPassword }
     * 
     */
    public DeleteUserByLoginAndPassword createDeleteUserByLoginAndPassword() {
        return new DeleteUserByLoginAndPassword();
    }

    /**
     * Create an instance of {@link DeleteUserByLoginAndPasswordResponse }
     * 
     */
    public DeleteUserByLoginAndPasswordResponse createDeleteUserByLoginAndPasswordResponse() {
        return new DeleteUserByLoginAndPasswordResponse();
    }

    /**
     * Create an instance of {@link DeleteUserResponse }
     * 
     */
    public DeleteUserResponse createDeleteUserResponse() {
        return new DeleteUserResponse();
    }

    /**
     * Create an instance of {@link FindOneUser }
     * 
     */
    public FindOneUser createFindOneUser() {
        return new FindOneUser();
    }

    /**
     * Create an instance of {@link FindOneUserByLoginAndPassword }
     * 
     */
    public FindOneUserByLoginAndPassword createFindOneUserByLoginAndPassword() {
        return new FindOneUserByLoginAndPassword();
    }

    /**
     * Create an instance of {@link FindOneUserByLoginAndPasswordResponse }
     * 
     */
    public FindOneUserByLoginAndPasswordResponse createFindOneUserByLoginAndPasswordResponse() {
        return new FindOneUserByLoginAndPasswordResponse();
    }

    /**
     * Create an instance of {@link FindOneUserResponse }
     * 
     */
    public FindOneUserResponse createFindOneUserResponse() {
        return new FindOneUserResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link UpdateUser }
     * 
     */
    public UpdateUser createUpdateUser() {
        return new UpdateUser();
    }

    /**
     * Create an instance of {@link UpdateUserByRole }
     * 
     */
    public UpdateUserByRole createUpdateUserByRole() {
        return new UpdateUserByRole();
    }

    /**
     * Create an instance of {@link UpdateUserByRoleResponse }
     * 
     */
    public UpdateUserByRoleResponse createUpdateUserByRoleResponse() {
        return new UpdateUserByRoleResponse();
    }

    /**
     * Create an instance of {@link UpdateUserPassword }
     * 
     */
    public UpdateUserPassword createUpdateUserPassword() {
        return new UpdateUserPassword();
    }

    /**
     * Create an instance of {@link UpdateUserPasswordResponse }
     * 
     */
    public UpdateUserPasswordResponse createUpdateUserPasswordResponse() {
        return new UpdateUserPasswordResponse();
    }

    /**
     * Create an instance of {@link UpdateUserResponse }
     * 
     */
    public UpdateUserResponse createUpdateUserResponse() {
        return new UpdateUserResponse();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AbstractEntity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iendpoint.api.tm.alekseev.ru/", name = "abstractEntity")
    public JAXBElement<AbstractEntity> createAbstractEntity(AbstractEntity value) {
        return new JAXBElement<AbstractEntity>(_AbstractEntity_QNAME, AbstractEntity.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iendpoint.api.tm.alekseev.ru/", name = "addUser")
    public JAXBElement<AddUser> createAddUser(AddUser value) {
        return new JAXBElement<AddUser>(_AddUser_QNAME, AddUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddUserByLoginAndPassword }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iendpoint.api.tm.alekseev.ru/", name = "addUserByLoginAndPassword")
    public JAXBElement<AddUserByLoginAndPassword> createAddUserByLoginAndPassword(AddUserByLoginAndPassword value) {
        return new JAXBElement<AddUserByLoginAndPassword>(_AddUserByLoginAndPassword_QNAME, AddUserByLoginAndPassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddUserByLoginAndPasswordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iendpoint.api.tm.alekseev.ru/", name = "addUserByLoginAndPasswordResponse")
    public JAXBElement<AddUserByLoginAndPasswordResponse> createAddUserByLoginAndPasswordResponse(AddUserByLoginAndPasswordResponse value) {
        return new JAXBElement<AddUserByLoginAndPasswordResponse>(_AddUserByLoginAndPasswordResponse_QNAME, AddUserByLoginAndPasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddUserByLoginPasswordUserRole }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iendpoint.api.tm.alekseev.ru/", name = "addUserByLoginPasswordUserRole")
    public JAXBElement<AddUserByLoginPasswordUserRole> createAddUserByLoginPasswordUserRole(AddUserByLoginPasswordUserRole value) {
        return new JAXBElement<AddUserByLoginPasswordUserRole>(_AddUserByLoginPasswordUserRole_QNAME, AddUserByLoginPasswordUserRole.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddUserByLoginPasswordUserRoleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iendpoint.api.tm.alekseev.ru/", name = "addUserByLoginPasswordUserRoleResponse")
    public JAXBElement<AddUserByLoginPasswordUserRoleResponse> createAddUserByLoginPasswordUserRoleResponse(AddUserByLoginPasswordUserRoleResponse value) {
        return new JAXBElement<AddUserByLoginPasswordUserRoleResponse>(_AddUserByLoginPasswordUserRoleResponse_QNAME, AddUserByLoginPasswordUserRoleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iendpoint.api.tm.alekseev.ru/", name = "addUserResponse")
    public JAXBElement<AddUserResponse> createAddUserResponse(AddUserResponse value) {
        return new JAXBElement<AddUserResponse>(_AddUserResponse_QNAME, AddUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iendpoint.api.tm.alekseev.ru/", name = "deleteUser")
    public JAXBElement<DeleteUser> createDeleteUser(DeleteUser value) {
        return new JAXBElement<DeleteUser>(_DeleteUser_QNAME, DeleteUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteUserByLoginAndPassword }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iendpoint.api.tm.alekseev.ru/", name = "deleteUserByLoginAndPassword")
    public JAXBElement<DeleteUserByLoginAndPassword> createDeleteUserByLoginAndPassword(DeleteUserByLoginAndPassword value) {
        return new JAXBElement<DeleteUserByLoginAndPassword>(_DeleteUserByLoginAndPassword_QNAME, DeleteUserByLoginAndPassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteUserByLoginAndPasswordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iendpoint.api.tm.alekseev.ru/", name = "deleteUserByLoginAndPasswordResponse")
    public JAXBElement<DeleteUserByLoginAndPasswordResponse> createDeleteUserByLoginAndPasswordResponse(DeleteUserByLoginAndPasswordResponse value) {
        return new JAXBElement<DeleteUserByLoginAndPasswordResponse>(_DeleteUserByLoginAndPasswordResponse_QNAME, DeleteUserByLoginAndPasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iendpoint.api.tm.alekseev.ru/", name = "deleteUserResponse")
    public JAXBElement<DeleteUserResponse> createDeleteUserResponse(DeleteUserResponse value) {
        return new JAXBElement<DeleteUserResponse>(_DeleteUserResponse_QNAME, DeleteUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iendpoint.api.tm.alekseev.ru/", name = "findOneUser")
    public JAXBElement<FindOneUser> createFindOneUser(FindOneUser value) {
        return new JAXBElement<FindOneUser>(_FindOneUser_QNAME, FindOneUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneUserByLoginAndPassword }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iendpoint.api.tm.alekseev.ru/", name = "findOneUserByLoginAndPassword")
    public JAXBElement<FindOneUserByLoginAndPassword> createFindOneUserByLoginAndPassword(FindOneUserByLoginAndPassword value) {
        return new JAXBElement<FindOneUserByLoginAndPassword>(_FindOneUserByLoginAndPassword_QNAME, FindOneUserByLoginAndPassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneUserByLoginAndPasswordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iendpoint.api.tm.alekseev.ru/", name = "findOneUserByLoginAndPasswordResponse")
    public JAXBElement<FindOneUserByLoginAndPasswordResponse> createFindOneUserByLoginAndPasswordResponse(FindOneUserByLoginAndPasswordResponse value) {
        return new JAXBElement<FindOneUserByLoginAndPasswordResponse>(_FindOneUserByLoginAndPasswordResponse_QNAME, FindOneUserByLoginAndPasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iendpoint.api.tm.alekseev.ru/", name = "findOneUserResponse")
    public JAXBElement<FindOneUserResponse> createFindOneUserResponse(FindOneUserResponse value) {
        return new JAXBElement<FindOneUserResponse>(_FindOneUserResponse_QNAME, FindOneUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Session }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iendpoint.api.tm.alekseev.ru/", name = "session")
    public JAXBElement<Session> createSession(Session value) {
        return new JAXBElement<Session>(_Session_QNAME, Session.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iendpoint.api.tm.alekseev.ru/", name = "updateUser")
    public JAXBElement<UpdateUser> createUpdateUser(UpdateUser value) {
        return new JAXBElement<UpdateUser>(_UpdateUser_QNAME, UpdateUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserByRole }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iendpoint.api.tm.alekseev.ru/", name = "updateUserByRole")
    public JAXBElement<UpdateUserByRole> createUpdateUserByRole(UpdateUserByRole value) {
        return new JAXBElement<UpdateUserByRole>(_UpdateUserByRole_QNAME, UpdateUserByRole.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserByRoleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iendpoint.api.tm.alekseev.ru/", name = "updateUserByRoleResponse")
    public JAXBElement<UpdateUserByRoleResponse> createUpdateUserByRoleResponse(UpdateUserByRoleResponse value) {
        return new JAXBElement<UpdateUserByRoleResponse>(_UpdateUserByRoleResponse_QNAME, UpdateUserByRoleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserPassword }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iendpoint.api.tm.alekseev.ru/", name = "updateUserPassword")
    public JAXBElement<UpdateUserPassword> createUpdateUserPassword(UpdateUserPassword value) {
        return new JAXBElement<UpdateUserPassword>(_UpdateUserPassword_QNAME, UpdateUserPassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserPasswordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iendpoint.api.tm.alekseev.ru/", name = "updateUserPasswordResponse")
    public JAXBElement<UpdateUserPasswordResponse> createUpdateUserPasswordResponse(UpdateUserPasswordResponse value) {
        return new JAXBElement<UpdateUserPasswordResponse>(_UpdateUserPasswordResponse_QNAME, UpdateUserPasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iendpoint.api.tm.alekseev.ru/", name = "updateUserResponse")
    public JAXBElement<UpdateUserResponse> createUpdateUserResponse(UpdateUserResponse value) {
        return new JAXBElement<UpdateUserResponse>(_UpdateUserResponse_QNAME, UpdateUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link User }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iendpoint.api.tm.alekseev.ru/", name = "user")
    public JAXBElement<User> createUser(User value) {
        return new JAXBElement<User>(_User_QNAME, User.class, null, value);
    }

}
