package ru.alekseev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.iendpoint.IProjectEndpoint;
import ru.alekseev.tm.api.iendpoint.Project;
import ru.alekseev.tm.api.iendpoint.Session;
import ru.alekseev.tm.command.system.AbstractCommand;

import java.util.List;

public final class ProjectListCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "show-projects";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Show list of all projects";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[LIST OF ALL PROJECTS]");
        @NotNull final IProjectEndpoint projectEndpoint =
                serviceLocator.getProjectEndpointService().getProjectEndpointPort();
        Session currentSession = serviceLocator.getCurrentSession();
        if (currentSession == null) return;
        if (currentSession.getUserId() == null || currentSession.getUserId().isEmpty()) return;

        @Nullable final List<Project> list = projectEndpoint.findAllProjectsByUserId(currentSession);
        if (list.size() == 0) {
            System.out.println("LIST OF PROJECTS IS EMPTY");
        }
        for (int i = 0; i < list.size(); i++) {
            System.out.print(i + 1);
            System.out.println(") name:" + list.get(i).getName() + ", projectId: " + list.get(i).getId());
        }
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
