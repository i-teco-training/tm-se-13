package ru.alekseev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.api.iendpoint.IProjectEndpoint;
import ru.alekseev.tm.api.iendpoint.ITaskEndpoint;
import ru.alekseev.tm.api.iendpoint.Session;
import ru.alekseev.tm.command.system.AbstractCommand;

public final class ProjectDeleteCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "delete-project";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Delete project by id";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[DELETING OF PROJECT]");
        @NotNull final IProjectEndpoint projectEndpoint =
                serviceLocator.getProjectEndpointService().getProjectEndpointPort();
        @NotNull final ITaskEndpoint taskEndpoint =
                serviceLocator.getTaskEndpointService().getTaskEndpointPort();

        Session currentSession = serviceLocator.getCurrentSession();
        if (currentSession == null) return;
        if (currentSession.getUserId() == null || currentSession.getUserId().isEmpty()) return;

        System.out.println("ENTER PROJECT ID");
        @NotNull final String projectId = serviceLocator.getTerminalService().getFromConsole();
        if (projectId.isEmpty()) return;
        @NotNull final String currentUserId = currentSession.getUserId();
        projectEndpoint.deleteProjectByProjectId(currentSession, projectId);
        //taskEndpoint.clearTasksByUserIdProjectId(currentSession, projectId);

        System.out.println("[OK. PROJECT AND IT'S TASKS DELETED]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
