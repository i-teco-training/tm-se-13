package ru.alekseev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.iendpoint.IProjectEndpoint;
import ru.alekseev.tm.api.iendpoint.Session;
import ru.alekseev.tm.command.system.AbstractCommand;

public final class ProjectAddCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "add-project";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Add new project";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[ADDING OF NEW PROJECT]");
        @NotNull final IProjectEndpoint projectEndpoint =
                serviceLocator.getProjectEndpointService().getProjectEndpointPort();
        @Nullable final Session currentSession = serviceLocator.getCurrentSession();
        if (currentSession == null) return;

        System.out.println("ENTER NAME");
        @NotNull final String projectName = serviceLocator.getTerminalService().getFromConsole();
        if (projectName.isEmpty()) {
            System.out.println("invalid input!");
            return;
        }

        projectEndpoint.addProjectByUserIdProjectName(currentSession, projectName);
        System.out.println("[OK]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
