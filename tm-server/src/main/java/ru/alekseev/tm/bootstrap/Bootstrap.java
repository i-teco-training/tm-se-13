package ru.alekseev.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.api.iservice.*;
import ru.alekseev.tm.endpoint.*;
import ru.alekseev.tm.enumerated.RoleType;
import ru.alekseev.tm.service.*;
import ru.alekseev.tm.util.HashUtil;

import javax.xml.ws.Endpoint;

public final class Bootstrap implements ServiceLocator {
    @NotNull private final IProjectService projectService = new ProjectService();
    @NotNull private final ITaskService taskService = new TaskService();
    @NotNull private final IUserService userService = new UserService();
    @NotNull private final ISessionService sessionService = new SessionService();
    @NotNull private final IDomainService domainService = new DomainService();

    @Override
    @NotNull
    public final IProjectService getProjectService() {
        return projectService;
    }

    @Override
    @NotNull
    public final ITaskService getTaskService() {
        return taskService;
    }

    @Override
    @NotNull
    public final IUserService getUserService() {
        return userService;
    }

    @Override
    @NotNull
    public ISessionService getSessionService() {
        return sessionService;
    }

    @Override
    @NotNull
    public final IDomainService getDomainService() {
        return domainService;
    }

    @NotNull public final static String PROJECT_ENDPOINT_WSDL = "http://localhost:8080/ProjectEndpoint?wsdl";
    @NotNull public final static String SESSION_ENDPOINT_WSDL = "http://localhost:8080/SessionEndpoint?wsdl";
    @NotNull public final static String TASK_ENDPOINT_WSDL = "http://localhost:8080/TaskEndpoint?wsdl";
    @NotNull public final static String USER_ENDPOINT_WSDL = "http://localhost:8080/UserEndpoint?wsdl";
    @NotNull public final static String DOMAIN_ENDPOINT_WSDL = "http://localhost:8080/DomainEndpoint?wsdl";

    public final void start() {
        try {
            //initUsers();
            initEndpoints();
        } catch (Exception e) {
            System.out.println("exception was thrown");
            e.printStackTrace();
        }
    }

//    public final void initUsers() {
//        this.userService.addByLoginPasswordUserRole("u1", HashUtil.getMd5("www"), RoleType.ADMIN);
//        this.userService.addByLoginPasswordUserRole("u2", HashUtil.getMd5("www"), RoleType.USER);
//    }

    public final void initEndpoints() {
        ProjectEndpoint projectEndpoint = new ProjectEndpoint(this);
        TaskEndpoint taskEndpoint = new TaskEndpoint(this);
        UserEndpoint userEndpoint = new UserEndpoint(this);
        SessionEndpoint sessionEndpoint = new SessionEndpoint(this);
        DomainEndpoint domainEndpoint = new DomainEndpoint(this);

//        projectEndpoint.setServiceLocator(this);
//        taskEndpoint.setServiceLocator(this);
//        userEndpoint.setServiceLocator(this);
//        sessionEndpoint.setServiceLocator(this);
//        domainEndpoint.setServiceLocator(this);

        Endpoint.publish(PROJECT_ENDPOINT_WSDL, projectEndpoint);
        Endpoint.publish(TASK_ENDPOINT_WSDL, taskEndpoint);
        Endpoint.publish(USER_ENDPOINT_WSDL, userEndpoint);
        Endpoint.publish(SESSION_ENDPOINT_WSDL, sessionEndpoint);
        Endpoint.publish(DOMAIN_ENDPOINT_WSDL, domainEndpoint);
    }
}