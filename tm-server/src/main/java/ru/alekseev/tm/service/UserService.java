package ru.alekseev.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.iservice.IUserService;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.enumerated.RoleType;
import ru.alekseev.tm.repository.IUserRepository;
import ru.alekseev.tm.util.SqlSessionFactoryUtil;

@Getter
@Setter
public class UserService implements IUserService {
    @Nullable private final SqlSessionFactory sessionFactory = SqlSessionFactoryUtil.getSqlSession();

    @Nullable
    public final User findOne(@NotNull final String id) {
        if (sessionFactory == null) return null;
        @Nullable final SqlSession session = sessionFactory.openSession();
        if (session == null) return null;
        @Nullable final IUserRepository userRepository = session.getMapper(IUserRepository.class);
        if (userRepository == null) return null;
        try {
            @Nullable User user = userRepository.findOne(id);
            session.commit();
            return user;
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        } finally {
            session.close();
        }
        return null;
    }

    @Override
    @Nullable
    public final User findOneByLoginAndPassword(@NotNull final String login, @NotNull final String passwordHashcode) {
        if (login.isEmpty() || passwordHashcode.isEmpty()) return null;
        if (sessionFactory == null) return null;
        @Nullable final SqlSession session = sessionFactory.openSession();
        if (session == null) return null;
        @Nullable final IUserRepository userRepository = session.getMapper(IUserRepository.class);
        if (userRepository == null) return null;
        try {
            @Nullable User user = userRepository.findOneByLoginAndPasswordHashcode(login,passwordHashcode);
            session.commit();
            return user;
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        } finally {
            session.close();
        }
        return null;
    }

    @Override
    public final void add(@NotNull final User user) {
        if (sessionFactory == null) return;
        @Nullable final SqlSession session = sessionFactory.openSession();
        if (session == null) return;
        @Nullable final IUserRepository userRepository = session.getMapper(IUserRepository.class);
        if (userRepository == null) return;
        try {
            userRepository.persist(user);
            session.commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public final void addByLoginPasswordUserRole(
            @NotNull final String login,
            @NotNull final String passwordHashcode,
            @NotNull final RoleType roleType
    ) {
        if (login.isEmpty() || passwordHashcode.isEmpty() || roleType.toString().isEmpty()) return;
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHashcode(passwordHashcode);
        user.setRoleType(roleType);
        add(user);
    }

    @Override
    public final void addByLoginAndPassword(
            @NotNull final String login,
            @NotNull final String passwordHashcode
    ) {
        if (login.isEmpty() || passwordHashcode.isEmpty()) return;
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHashcode(passwordHashcode);
        add(user);
    }

    @Override
    public final void update(@NotNull final User user) {
        if (sessionFactory == null) return;
        @Nullable final SqlSession session = sessionFactory.openSession();
        if (session == null) return;
        @Nullable final IUserRepository userRepository = session.getMapper(IUserRepository.class);
        if (userRepository == null) return;
        try {
            userRepository.merge(user);
            session.commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public void updateUserPassword(@NotNull final String userId, @NotNull final String passwordHashcode) {
        if (sessionFactory == null) return;
        @Nullable final SqlSession session = sessionFactory.openSession();
        if (session == null) return;
        @Nullable final IUserRepository userRepository = session.getMapper(IUserRepository.class);
        if (userRepository == null) return;
        try {
            userRepository.updateUserPassword(userId, passwordHashcode);
            session.commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public void updateUserByRole(String userId, RoleType roleType) {
        if (sessionFactory == null) return;
        @Nullable final SqlSession session = sessionFactory.openSession();
        if (session == null) return;
        @Nullable final IUserRepository userRepository = session.getMapper(IUserRepository.class);
        if (userRepository == null) return;
        try {
            userRepository.updateUserByRole(userId, roleType);
            session.commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public final void delete(@NotNull final String id) {
        if (sessionFactory == null) return;
        @Nullable final SqlSession session = sessionFactory.openSession();
        if (session == null) return;
        @Nullable final IUserRepository userRepository = session.getMapper(IUserRepository.class);
        if (userRepository == null) return;
        try {
            userRepository.delete(id);
            session.commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public final void deleteByLoginAndPassword(@NotNull final String login, @NotNull final String passwordHashcode) {
        if (login.isEmpty() || passwordHashcode.isEmpty()) return;
        if (sessionFactory == null) return;
        @Nullable final SqlSession session = sessionFactory.openSession();
        if (session == null) return;
        @Nullable final IUserRepository userRepository = session.getMapper(IUserRepository.class);
        if (userRepository == null) return;
        try {
            userRepository.deleteByLoginAndPassword(login, passwordHashcode);
            session.commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        } finally {
            session.close();
        }
    }
}
