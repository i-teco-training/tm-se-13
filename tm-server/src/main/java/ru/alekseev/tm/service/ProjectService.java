package ru.alekseev.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.iservice.IProjectService;
import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.repository.IProjectRepository;
import ru.alekseev.tm.util.SqlSessionFactoryUtil;

import java.util.List;

public final class ProjectService implements IProjectService {
    @Nullable private final SqlSessionFactory sessionFactory = SqlSessionFactoryUtil.getSqlSession();

    @Override
    @Nullable
    public final Project findOneByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (sessionFactory == null) return null;
        @Nullable final SqlSession session = sessionFactory.openSession();
        if (session == null) return null;
        @Nullable final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
        if (projectRepository == null) return null;
        try {
            @Nullable final Project project = projectRepository.findOneByUserIdAndProjectId(userId, projectId);
            session.commit();
            return project;
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        } finally {
            session.close();
        }
        return null;
    }

    @Override
    @Nullable
    public final List<Project> findAllByUserId(@NotNull final String userId) {
        if (userId.isEmpty()) return null;
        if (sessionFactory == null) return null;
        @Nullable final SqlSession session = sessionFactory.openSession();
        if (session == null) return null;
        @Nullable final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
        if (projectRepository == null) return null;
        @NotNull List<Project> projects = null;
        try {
            projects = projectRepository.findAllByUserId(userId);
            session.commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        } finally {
            session.close();
        }
        return projects;
    }


    public final void add(@NotNull final Project project) {
        if (sessionFactory == null) return;
        @Nullable final SqlSession session = sessionFactory.openSession();
        if (session == null) return;
        @Nullable final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
        try {
            projectRepository.persist(project);
            session.commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public void addProjectByUserIdProjectName(String userId, String projectName) {
        @NotNull final Project project = new Project();
        project.setUserId(userId);
        project.setName(projectName);
        add(project);
    }

    @Override
    public final void updateByUserIdProjectIdProjectName(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String projectName
    ) {
        if (userId.isEmpty() || projectId.isEmpty() || projectName.isEmpty()) return;
        if (sessionFactory == null) return;
        @Nullable final SqlSession session = sessionFactory.openSession();
        if (session == null) return;
        @Nullable final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
        try {
            projectRepository.updateByUserIdProjectIdProjectName(userId, projectId, projectName);
            session.commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        } finally {
            session.close();
        }
    }

    public final void delete(@NotNull final String id) {
        if (sessionFactory == null) return;
        @Nullable final SqlSession session = sessionFactory.openSession();
        if (session == null) return;
        @Nullable final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
        try {
            projectRepository.delete(id);
            session.commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public final void deleteByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (userId.isEmpty() || projectId.isEmpty()) return;
        if (sessionFactory == null) return;
        @Nullable final SqlSession session = sessionFactory.openSession();
        if (session == null) return;
        @Nullable final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
        try {
            @Nullable Project projectForExistenceChecking = projectRepository.findOne(projectId);
            if (projectForExistenceChecking.getUserId() == null || projectForExistenceChecking.getUserId().isEmpty())
                return;
            if (!userId.equals(projectForExistenceChecking.getUserId())) return;
            delete(projectId);
            session.commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public final void clearByUserId(@NotNull final String userId) {
        if (userId.isEmpty()) return;
        if (sessionFactory == null) return;
        @Nullable final SqlSession session = sessionFactory.openSession();
        if (session == null) return;
        @Nullable final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
        try {
            projectRepository.deleteByUserId(userId);
            session.commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        } finally {
            session.close();
        }
    }
}
