package ru.alekseev.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.iservice.ISessionService;
import ru.alekseev.tm.entity.Session;
import ru.alekseev.tm.repository.ISessionRepository;
import ru.alekseev.tm.util.CreateSessionUtil;
import ru.alekseev.tm.util.SignatureUtil;
import ru.alekseev.tm.util.SqlSessionFactoryUtil;

public final class SessionService implements ISessionService {
    @Nullable private final SqlSessionFactory sessionFactory = SqlSessionFactoryUtil.getSqlSession();

    @Override
    public final Session open(@NotNull final String userId) {
        @NotNull Session session = CreateSessionUtil.createSession(userId);
        if (sessionFactory == null) return null;
        @Nullable final SqlSession sqlSession = sessionFactory.openSession();
        if (sqlSession == null) return null;
        @Nullable final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        if (sessionRepository == null) return null;
        try {
            sessionRepository.persist(session);
            sqlSession.commit();
            return session;
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
        return null;
    }

    @Override
    public final boolean isValid(@NotNull final Session session) {
        if (session == null) return false;
        if (session.getSignature() == null || session.getSignature().isEmpty()) return false;
        if (session.getUserId() == null || session.getUserId().isEmpty()) return false;
        if (session.getTimestamp() == null) return false;
        @NotNull final String sourceSignature = session.getSignature();
        session.setSignature(null);
        @NotNull final String targetSignature = SignatureUtil.sign(session);
        return sourceSignature.equals(targetSignature);
    }

    public void delete(String id) {
        if (sessionFactory == null) return;
        @Nullable final SqlSession sqlSession = sessionFactory.openSession();
        if (sqlSession == null) return;
        @Nullable final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        if (sessionRepository == null) return;
        try {
            sessionRepository.closeSession(id);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
    }
}
