package ru.alekseev.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.iservice.ITaskService;
import ru.alekseev.tm.entity.Task;
import ru.alekseev.tm.repository.ITaskRepository;
import ru.alekseev.tm.util.DBConnectionUtil;
import ru.alekseev.tm.util.SqlSessionFactoryUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class TaskService implements ITaskService {
    @Nullable private final SqlSessionFactory sessionFactory = SqlSessionFactoryUtil.getSqlSession();

    @Override
    @Nullable
    public final Task findOneByUserIdAndTaskId(@NotNull final String userId, @NotNull final String taskId) {
        if (sessionFactory == null) return null;
        @Nullable final SqlSession session = sessionFactory.openSession();
        if (session == null) return null;
        @Nullable final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
        if (taskRepository == null) return null;
        try {
            session.commit();
            return taskRepository.findOneByUserIdAndTaskId(userId, taskId);
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        } finally {
            session.close();
        }
        return null;
    }

    @Override
    @Nullable
    public final List<Task> findAllByUserId(@NotNull final String userId) {
        if (sessionFactory == null) return null;
        @Nullable final SqlSession session = sessionFactory.openSession();
        if (session == null) return null;
        @Nullable final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
        if (taskRepository == null) return null;
        try {
            session.commit();
            return taskRepository.findAllByUserId(userId);
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        } finally {
            session.close();
        }
        return null;
    }

    public final void add(@NotNull final Task task) {
        if (sessionFactory == null) return;
        @Nullable final SqlSession session = sessionFactory.openSession();
        if (session == null) return;
        @Nullable final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
        if (taskRepository == null) return;
        try {
            taskRepository.persist(task);
            session.commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public final void addTaskByUserIdTaskName(@NotNull final String userId, @NotNull final String taskName) {
        @NotNull final Task task = new Task();
        task.setUserId(userId);
        task.setName(taskName);
        add(task);
    }

    @Override
    public final void updateByNewData(
            @NotNull final String userId,
            @NotNull final String taskId,
            @NotNull final String name
    ) {
        if (userId.isEmpty() || taskId.isEmpty() || name.isEmpty()) return;
        @Nullable final SqlSession session = sessionFactory.openSession();
        if (session == null) return;
        @Nullable final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
        if (taskRepository == null) return;
        try {
            taskRepository.updateByNewData(userId, taskId, name);
            session.commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public final void delete(@NotNull final String id) {
        if (id.isEmpty()) return;
        @Nullable final SqlSession session = sessionFactory.openSession();
        if (session == null) return;
        @Nullable final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
        if (taskRepository == null) return;
        try {
            taskRepository.delete(id);
            session.commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public final void deleteByUserIdAndTaskId(@NotNull final String userId, @NotNull final String taskId) {
        if (userId.isEmpty() || taskId.isEmpty()) return;
        @Nullable final SqlSession session = sessionFactory.openSession();
        if (session == null) return;
        @Nullable final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
        if (taskRepository == null) return;
        try {
            taskRepository.deleteByUserIdAndTaskId(userId, taskId);
            session.commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        } finally {
            session.close();
        }
    }
}
