package ru.alekseev.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.iservice.IDomainService;
import ru.alekseev.tm.dto.Domain;
import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.entity.Task;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.repository.IProjectRepository;
import ru.alekseev.tm.repository.ITaskRepository;
import ru.alekseev.tm.repository.IUserRepository;
import ru.alekseev.tm.util.SqlSessionFactoryUtil;

import java.util.List;

public final class DomainService implements IDomainService {
    @Nullable private final SqlSessionFactory sessionFactory = SqlSessionFactoryUtil.getSqlSession();

    @Override
    @NotNull
    public final Domain getDomain() {
        if (sessionFactory == null) return null;
        @Nullable final SqlSession sqlSession = sessionFactory.openSession();
        if (sqlSession == null) return null;
        @Nullable final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @Nullable final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        @Nullable final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        if (projectRepository == null || userRepository == null || taskRepository == null) return null;
        try {
            @Nullable final List<Project> projects = projectRepository.findAll();
            @Nullable final List<Task> tasks = taskRepository.findAll();
            @Nullable final List<User> users = userRepository.findAll();
            @NotNull final Domain domain = new Domain();
            domain.setProjects(projects);
            domain.setTasks(tasks);
            domain.setUsers(users);
            sqlSession.commit();
            return domain;
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
        return null;
    }

    @Override
    public final void setDomain(@Nullable final Domain domain) {
        if (sessionFactory == null) return;
        @Nullable final SqlSession sqlSession = sessionFactory.openSession();
        if (sqlSession == null) return;
        @Nullable final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @Nullable final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        @Nullable final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        if (projectRepository == null || userRepository == null || taskRepository == null) return;
        try {
            projectRepository.clear();
            projectRepository.addAll(domain.getProjects());
            taskRepository.clear();
            taskRepository.addAll(domain.getTasks());
            userRepository.clear();
            userRepository.addAll(domain.getUsers());
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
    }
}
