package ru.alekseev.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.api.iendpoint.ITaskEndpoint;
import ru.alekseev.tm.entity.Session;
import ru.alekseev.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@WebService(endpointInterface = "ru.alekseev.tm.api.iendpoint.ITaskEndpoint")
public class TaskEndpoint implements ITaskEndpoint {
    private ServiceLocator serviceLocator;

    public TaskEndpoint(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public Task findOneTaskByUserIdAndTaskId(@NotNull final String userId, @NotNull final String taskId) {
        return serviceLocator.getTaskService().findOneByUserIdAndTaskId(userId, taskId);
    }

    @Override
    @Nullable
    @WebMethod
    public List<Task> findAllTasksByUserId(@WebParam @NotNull final Session session) {
        return serviceLocator.getTaskService().findAllByUserId(session.getUserId());
    }

    @Override
    @WebMethod
    public void addTask(@WebParam @NotNull final Task entity) {
        serviceLocator.getTaskService().add(entity);
    }

    @Override
    @WebMethod
    public void addTaskByUserIdTaskName(
            @WebParam @NotNull final Session session,
            @WebParam @NotNull final String taskName
    ) {
        if (!serviceLocator.getSessionService().isValid(session)) {
            serviceLocator.getSessionService().delete(session.getId());
            return;
        }
        serviceLocator.getTaskService().addTaskByUserIdTaskName(session.getUserId(), taskName);
    }

    @Override
    @WebMethod
    public void updateTaskByTaskIdTaskName(
            @WebParam @NotNull final Session session,
            @WebParam @NotNull final String taskId,
            @WebParam @NotNull final String name
    ) {
        if (!serviceLocator.getSessionService().isValid(session)) {
            serviceLocator.getSessionService().delete(session.getId());
            return;
        }
        serviceLocator.getTaskService().updateByNewData(session.getUserId(), taskId, name);
    }

    @Override
    @WebMethod
    public void deleteTaskByTaskId(
            @WebParam @NotNull final Session session,
            @WebParam @NotNull final String taskId
    ) {
        if (!serviceLocator.getSessionService().isValid(session)) {
            serviceLocator.getSessionService().delete(session.getId());
            return;
        }
        serviceLocator.getTaskService().deleteByUserIdAndTaskId(session.getUserId(), taskId);
    }

    @Override
    @WebMethod
    public void deleteTask(@WebParam @NotNull final String id) {
        serviceLocator.getTaskService().delete(id);
    }
}
