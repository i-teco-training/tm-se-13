package ru.alekseev.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.api.iendpoint.IProjectEndpoint;
import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@WebService(endpointInterface = "ru.alekseev.tm.api.iendpoint.IProjectEndpoint")
public class ProjectEndpoint {
    private ServiceLocator serviceLocator;

    public ProjectEndpoint(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    public Project findOneProjectByUserIdAndProjectId(
            @WebParam @NotNull final String userId,
            @WebParam @NotNull final String projectId
    ) {
        return serviceLocator.getProjectService().findOneByUserIdAndProjectId(userId, projectId);
    }

    @WebMethod
    public List<Project> findAllProjectsByUserId(@WebParam @NotNull final Session session) {
        return serviceLocator.getProjectService().findAllByUserId(session.getUserId());
    }

    @WebMethod
    public void addProjectByUserIdProjectName(
            @WebParam @NotNull final Session session,
            @WebParam @NotNull final String projectName
    ) {
        if (!serviceLocator.getSessionService().isValid(session)) {
            serviceLocator.getSessionService().delete(session.getId());
            return;
        }
        serviceLocator.getProjectService().addProjectByUserIdProjectName(session.getUserId(), projectName);
    }

    @WebMethod
    public void updateProjectByProjectIdProjectName(
            @WebParam @NotNull final Session session,
            @WebParam @NotNull final String projectId,
            @WebParam @NotNull final String name
    ) {
        if (!serviceLocator.getSessionService().isValid(session)) {
            serviceLocator.getSessionService().delete(session.getId());
            return;
        }
        serviceLocator.getProjectService().updateByUserIdProjectIdProjectName(session.getUserId(), projectId, name);
    }

    @WebMethod
    public void deleteProjectByProjectId(
            @WebParam @NotNull final Session session,
            @WebParam @NotNull final String projectId
    ) {
        if (!serviceLocator.getSessionService().isValid(session)) {
            serviceLocator.getSessionService().delete(session.getId());
            return;
        }
        serviceLocator.getProjectService().deleteByProjectId(session.getUserId(), projectId);
    }


    @WebMethod
    public void clearProjectsByUserId(@WebParam @NotNull final String userId) {
        serviceLocator.getProjectService().clearByUserId(userId);
    }


    @WebMethod
    public void updateProject(@WebParam @NotNull final Project entity) {
    }


    @WebMethod
    public void deleteProject(@WebParam @NotNull final String id) {
    }


    @WebMethod
    public List<Project> findAllProjects() {
        return null;
    }

    @WebMethod
    public void clearProjects() {
    }

}
