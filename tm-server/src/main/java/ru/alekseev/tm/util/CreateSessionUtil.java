package ru.alekseev.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.entity.Session;

public final class CreateSessionUtil {
    public final static Session createSession(String userId) {
        @NotNull final Session newSession = new Session();
        newSession.setUserId(userId);
        @NotNull final String signature = SignatureUtil.sign(newSession);
        newSession.setSignature(signature);
        return newSession;
    }
}
