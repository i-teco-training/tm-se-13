package ru.alekseev.tm.util;


import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.Reader;


public class SqlSessionFactoryUtil {
    @Nullable
    public static SqlSessionFactory getSqlSession() {
        @Nullable SqlSessionFactory sqlSessionFactory = null;
        Reader reader;
        try {
            reader = Resources.getResourceAsReader("mybatis-config.xml");
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sqlSessionFactory;
    }
}
