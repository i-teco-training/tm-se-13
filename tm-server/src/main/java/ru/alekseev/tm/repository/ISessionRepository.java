package ru.alekseev.tm.repository;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.entity.Session;

public interface ISessionRepository {

    @Insert(
            "INSERT INTO app_session (id, user_id, signature) VALUES (#{id},#{userId},#{signature})"
    )
    void persist(@NotNull final Session session);



    @Delete(
            "DELETE FROM app_session WHERE id = #{arg0}"
    )
    void closeSession(String id);
}
