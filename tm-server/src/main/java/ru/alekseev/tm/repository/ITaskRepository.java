package ru.alekseev.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {

    @Select(
            "SELECT * FROM app_task WHERE user_id = #{userId} AND id = #{taskId}"
    )
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "createdOn", column = "createdOn"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "task_status"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable
    Task findOneByUserIdAndTaskId(@NotNull final String userId, @NotNull final String taskId);


    @Select(
            "SELECT * FROM app_task"
    )
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "createdOn", column = "createdOn"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "task_status"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable
    List<Task> findAll();


    @Select(
            "SELECT * FROM app_task WHERE user_id = #{userId}"
    )
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "createdOn", column = "createdOn"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "task_status"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable
    List<Task> findAllByUserId(@NotNull final String userId);


    @Insert(
            "INSERT INTO app_task (id, createdOn, dateBegin, dateEnd, description, name, task_status, user_id, project_id) "
                    + "VALUES (#{id},#{createdOn},#{dateStart},#{dateFinish},#{description},#{name},#{status},#{userId},#{projectId})"
    )
    void persist(@NotNull final Task task);


    @Insert(
            "INSERT INTO app_task (id, createdOn, dateBegin, dateEnd, description, name, task_status, user_id, project_id) "
                    + "VALUES (#{id},#{createdOn},#{dateStart},#{dateFinish},#{description},#{name},#{status},#{userId},#{projectId})"
    )
    void addAll(@NotNull final List<Task> tasks);

    @Update(
            "UPDATE app_task SET name = #{name} WHERE id = #{taskId}, user_id = #{userId}"
    )
    void updateByNewData(@NotNull final String userId, @NotNull final String taskId, @NotNull final String name);


    @Delete(
            "DELETE FROM app_task WHERE id = #{id}"
    )
    void delete(@NotNull String id);


    @Delete(
            "DELETE FROM app_task WHERE user_id = #{userId}, id = #{taskId}"
    )
    void deleteByUserIdAndTaskId(@NotNull final String userId, @NotNull final String taskId);


    @Delete(
            "DELETE FROM app_task"
    )
    void clear();
}
