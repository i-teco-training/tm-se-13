package ru.alekseev.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {
    @Select(
            "SELECT * FROM app_project WHERE id = #{id}"
    )
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "createdOn", column = "createdOn"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "project_status"),
            @Result(property = "userId", column = "user_id")
    })
    @Nullable
    Project findOne(@NotNull final String id);


    @Select(
            "SELECT * FROM app_project WHERE user_id = #{userId} AND id = #{projectId}" //ok про projectId????
    )
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "createdOn", column = "createdOn"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "project_status"),
            @Result(property = "userId", column = "user_id")
    })
    @Nullable
    Project findOneByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);


    @Select(
            "SELECT * FROM app_project"
    )
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "createdOn", column = "createdOn"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "project_status"),
            @Result(property = "userId", column = "user_id")
    })
    @Nullable
    List<Project> findAll();


    @Select(
            "SELECT * FROM app_project WHERE user_id = #{userId}"
    )
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "createdOn", column = "createdOn"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "project_status"),
            @Result(property = "userId", column = "user_id")
    })
    @Nullable
    List<Project> findAllByUserId(@NotNull final String userId);


    @Insert(
            "INSERT INTO app_project (id, createdOn, dateBegin, dateEnd, description, name, project_status, user_id) "
                    + "VALUES (#{id},#{createdOn},#{dateStart},#{dateFinish},#{description},#{name},#{status},#{userId})"
    )
    void persist(@NotNull final Project project);


    @Insert(
            "INSERT INTO app_project (id, createdOn, dateBegin, dateEnd, description, name, project_status, user_id) "
                    + "VALUES (#{id},#{createdOn},#{dateStart},#{dateFinish},#{description},#{name},#{status},#{userId})"
    )
    void addAll(@NotNull final List<Project> projects);


    @Update(
            "UPDATE app_project SET name = #{name} WHERE id = #{id}, user_id = #{userId}"
    )
     void updateByUserIdProjectIdProjectName(@NotNull final String userId, @NotNull final String id, @NotNull final String name);


    @Delete(
            "DELETE FROM app_project WHERE id = #{id}"
    )
    void delete(@NotNull String id);


    @Delete(
            "DELETE FROM app_project WHERE user_id = #{userId}"
    )
    void deleteByUserId(@NotNull final String userId);

    @Delete(
            "DELETE FROM app_project WHERE user_id = #{userId} AND id = #{id}"
    )
    void deleteProjectByProjectId(@NotNull final String userId, @NotNull final String id);


    @Delete(
            "DELETE FROM app_project"
    )
    void clear();
}
