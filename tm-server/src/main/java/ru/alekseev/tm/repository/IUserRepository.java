package ru.alekseev.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.enumerated.RoleType;

import java.util.List;

public interface IUserRepository {
    @Select(
            "SELECT * FROM app_user WHERE id = #{arg0}"
    )
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "passwordHashcode", column = "passwordHash"),
            @Result(property = "email", column = "email"),
            @Result(property = "roleType", column = "role")
    })
    @Nullable
    User findOne(@NotNull String id);


    @Select(
            "SELECT * FROM app_user WHERE login = #{arg0} AND passwordHash = #{arg1}"
    )
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "passwordHashcode", column = "passwordHash"),
            @Result(property = "email", column = "email"),
            @Result(property = "roleType", column = "role")
    })
    @Nullable
    User  findOneByLoginAndPasswordHashcode(String login, String passwordHashcode);


    @Select(
            "SELECT * FROM app_user"
    )
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "passwordHashcode", column = "passwordHash"),
            @Result(property = "email", column = "email"),
            @Result(property = "roleType", column = "role")
    })
    @Nullable
    List<User> findAll();


    @Insert(
            "INSERT INTO app_user (id, login, passwordHash, email, role) "
                    + "VALUES (#{id},#{login},#{passwordHashcode},#{email},#{roleType})"
    )
    void persist(@NotNull final User user);


    @Insert(
            "INSERT INTO app_user (id, login, passwordHash, email, role) "
                    + "VALUES (#{id},#{login},#{passwordHashcode},#{email},#{roleType})"
    )
    void addAll(@NotNull final List<User> users);


    @Update(
            "UPDATE app_user SET login = #{login}, passwordHash = #{passwordHashcode}, email = #{email}, " +
                    "role = #{roleType} WHERE id = #{id}"
    )
    void merge(@NotNull User user);


    @Update(
            "UPDATE app_user SET passwordHash = #{arg0} WHERE id = #{arg1}"
    )
    void updateUserPassword(@NotNull final String id, @NotNull final String passwordHashcode);


    @Update(
            "UPDATE app_user SET role = #{arg1} WHERE id = #{arg0}"
    )
    void updateUserByRole(@NotNull final String id, @NotNull final RoleType roleType);


    @Delete(
            "DELETE FROM app_user WHERE id = #{arg0}"
    )
    void delete(String id);


    @Delete(
            "DELETE FROM app_user WHERE login = #{arg0} AND passwordHash = #{arg1}"
    )
    void deleteByLoginAndPassword(String login, String passwordHashcode);


    @Delete(
            "DELETE FROM app_user"
    )
    void clear();
}
