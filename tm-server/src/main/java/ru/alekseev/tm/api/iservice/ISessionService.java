package ru.alekseev.tm.api.iservice;

import ru.alekseev.tm.entity.Session;

public interface ISessionService {

    Session open(String userId);

    boolean isValid(Session session);

    void delete(String id);
}
