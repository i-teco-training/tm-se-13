package ru.alekseev.tm.api.iservice;

import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.entity.Task;

import java.util.List;

public interface ITaskService {

    Task findOneByUserIdAndTaskId(String userId, String taskId);

    List<Task> findAllByUserId(String userId);

    void add(Task task);

    void addTaskByUserIdTaskName(String userId, String taskName);

    void updateByNewData(String userId, String taskId, String name);

    void delete(String id);

    void deleteByUserIdAndTaskId(String userId, String taskId);
}