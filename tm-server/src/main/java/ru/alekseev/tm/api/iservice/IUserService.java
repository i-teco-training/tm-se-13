package ru.alekseev.tm.api.iservice;

import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.enumerated.RoleType;

public interface IUserService {

    User findOne(String id);

    User findOneByLoginAndPassword(String login, String passwordHashcode);

    void add(User user);

    void addByLoginAndPassword(String login, String passwordHashcode);

    void addByLoginPasswordUserRole(String login, String passwordHashcode, RoleType roleType);

    void update(User user);

    void updateUserPassword(String userId, String passwordHashcode);

    void updateUserByRole(String userId, RoleType roleType);

    void delete(String id);

    void deleteByLoginAndPassword(String login, String passwordHashcode);
}
